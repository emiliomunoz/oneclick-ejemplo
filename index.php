<!doctype html>

<html lang="en">
<head>
  <meta charset="utf-8">

  <title>Oneclick test</title>
  <meta name="description" content="The HTML5 Herald">
  <meta name="author" content="SitePoint">

  <!--link rel="stylesheet" href="css/styles.css?v=1.0"-->

</head>

<body>

	<h1>Test de OneClick</h1>
	<p>Esta es una prueba de implementación de OneClick</p>

	<form action="http://localhost/oneclick/createInscription.php" method="post">
		<label for="username">Nombre de usuario:</label><input type="text" name="username" value="emiliomunoz"/> <p>Identificador del usuario en el comercio</p><br>
		<label for="email">Correo:</label><input type="email" name="email" value="emiliomunozmonge@gmail.com"/> <p>Correo electrónico del usuario</p><br>
		<input type="submit" name="submit" value="Crear Inscripción">
	</form>

  <!--script src="js/scripts.js"></script-->
</body>
</html>