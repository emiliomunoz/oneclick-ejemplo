<?php

require __DIR__ . '/vendor/autoload.php';

use Symfony\Component\Yaml\Yaml;
use Symfony\Component\HttpFoundation\Request;
use Transbank\Webpay\Oneclick;
use Transbank\Webpay\Oneclick\MallInscription;

$config = Yaml::parseFile( __DIR__ . '/config.yaml');

if($config["ambiente"] == "prod") {
	Oneclick::configureForProduction($config["tbkcommerceCode"], $config["tbkapiKeySecret"]);
} else {
	Oneclick::configureForIntegration($config["tbkcommerceCode"], $config["tbkapiKeySecret"]); // Esto no es necesario, por defecto va
}

$request = Request::createFromGlobals();
$response_url = $request->getScheme() ."://" . $request->getHost() . "/oneclick/confirmInscription.php"; //URL donde llegará el usuario con su token luego de finalizar la inscripción

$response = (new MallInscription)->start($_POST["username"], $_POST["email"], $response_url);

$url_webpay = $response->getUrlWebpay();
$token = $response->getToken();

header("Location: {$url_webpay}?TBK_TOKEN={$token}");

//var_dump($response);

// object(Transbank\Webpay\Oneclick\Responses\InscriptionStartResponse)[10]
//   public 'token' => string '01ab9e40b442564e68563d5382fa0df4eca6c7a79c8d1943a4d73fb649d8a9fe' (length=64)
//   public 'urlWebpay' => string 'https://webpay3gint.transbank.cl/webpayserver/bp_multicode_inscription.cgi' (length=74)