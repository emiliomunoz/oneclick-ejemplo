<?php

require __DIR__ . '/vendor/autoload.php';

use Symfony\Component\Yaml\Yaml;
use Transbank\Webpay\Oneclick;
use Transbank\Webpay\Oneclick\MallInscription;

$config = Yaml::parseFile( __DIR__ . '/config.yaml');

if($config["ambiente"] == "prod") {
	Oneclick::configureForProduction($config["tbkcommerceCode"], $config["tbkapiKeySecret"]);
} else {
	Oneclick::configureForIntegration($config["tbkcommerceCode"], $config["tbkapiKeySecret"]); // Esto no es necesario, por defecto va
}

$tbk_token = $_GET['TBK_TOKEN'];

$response = (new MallInscription)->finish($tbk_token);
$tbkUser = $response->getTbkUser();

if ($response->getResponseCode() === 0) {
	// Registro de datos de suscripción de pago
	// Redirección de mensaje de éxito
	echo "Se ha registrado tu tarjeta {$response->getCardType()} número {$response->getCardNumber()}";
} else {
	// Registro de error en suscripción
	// Redirección de mensaje de error
	echo "Ha ocurrido un error a registrar tu tarjeta.";
}

//var_dump($response);

//object(Transbank\Webpay\Oneclick\Responses\InscriptionFinishResponse)[9]
//  public 'responseCode' => int 0
//  public 'tbkUser' => string '708ae887-b2b4-4458-8294-924e15df4fd8' (length=36)
//  public 'authorizationCode' => string '1213' (length=4)
//  public 'cardType' => string 'Visa' (length=4)
//  public 'cardNumber' => string 'XXXXXXXXXXXX6623' (length=16)